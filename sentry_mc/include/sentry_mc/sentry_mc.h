/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_MC_H
#define SENTRY_MC_H

#include "ds_base/ds_process.h"
#include "ds_core_msgs/RawData.h"
#include "std_msgs/String.h"
#include "goal_bottom_follow_depth/goal_bottom_follow_depth.h"
#include "goal_depth_follow/goal_depth_follow.h"
#include "goal_leg/goal_leg.h"
#include "ds_nav_msgs/AggregatedState.h"
#include "ds_nav_msgs/Shift.h"
#include "boost/date_time/posix_time/posix_time.hpp"
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include "ds_core_msgs/Abort.h"
#include "sail_grd/pwr_lookup.h"
#include "ds_core_msgs/StringStamped.h"

// Services
#include "sentry_msgs/XRCmd.h"
#include "sentry_msgs/PWRCmd.h"
#include "sentry_msgs/SailServoCmd.h"
#include "ds_hotel_msgs/AbortCmd.h"
#include "ds_base/BoolCommand.h"
#include "ds_nav_msgs/ResetDvl.h"
#include "sentry_msgs/InitiateBallastTest.h"
#include "sentry_msgs/TimedJoystickService.h"
#include "sentry_msgs/SypridCmd.h"
#include "ds_supr/SuprCmd.h"
#include "ds_control_msgs/DepthFollow.h"

// Hotel
#include "ds_hotel_msgs/HTP.h"
#include "ds_hotel_msgs/BatMan.h"
#include "ds_hotel_msgs/XR.h"

// Allocations and controllers
#include "sentry_msgs/SentryAllocationEnum.h"
#include "sentry_msgs/SentryControllerEnum.h"
#include "sentry_msgs/SentryJoystickEnum.h"
#include "ds_control_msgs/JoystickEnum.h"

#include <actionlib/client/action_client.h>
#include <ds_kctrl_msgs/KctrlCmdAction.h>
#include <ds_kloggerd_msgs/KloggerdCmdAction.h>

#include <memory>
#include <list>

namespace sentry_mc
{
class SentryMc : public ds_base::DsProcess
{
public:
  SentryMc(int argc, char* argv[], const std::string& name);
  ~SentryMc() override;

  void processCommandData(std::string mcData);

  void onMcData(ds_core_msgs::RawData bytes);

  void onTimeData(ds_core_msgs::RawData bytes);

  void onJtRosMsg(const std_msgs::String::ConstPtr msg);

  void onAbortMsg(const ds_core_msgs::Abort::ConstPtr msg);

  void runAbortSequence(void);

  void onBottomFollowMsg(const ds_control_msgs::BottomFollow1D::ConstPtr msg);

  void onBatmanMsg(const ds_hotel_msgs::BatMan::ConstPtr msg);

  void onHtpMsg(const ds_hotel_msgs::HTP::ConstPtr msg, std::string number, std::string num_chars);

  void onXrMsg(const ds_hotel_msgs::XR::ConstPtr msg, std::string number);

  template <typename T>
  bool serviceRetry(ros::ServiceClient srvClient, T srv, int n_retries, std::string success, std::string failure);

  void onAggregatedStateMsg(const ds_nav_msgs::AggregatedState::ConstPtr msg);

protected:
  void setup() override;
  void setupConnections() override;
  void setupParameters() override;
  void setupPublishers() override;
  void setupSubscriptions() override;
  void setupServices() override;
  void setupActions();

  // Handle a command intended for Kongsberg's K-Controller (e.g. start/stop EM2040 pinging, change runtime parameters, etc.)
  void handleKctrlCommand(const std::string& str);
  // Handle a command intended for the kloggerd multibeam logging applicatoin (e.g. start/stop recording, change line number).
  void handleKloggerdCommand(const std::string& str);
  
private:

  void kctrlCommandGoalTransitionCallback(actionlib::ActionClient<ds_kctrl_msgs::KctrlCmdAction>::GoalHandle goal_handle);
  void kloggerdCommandGoalTransitionCallback(actionlib::ActionClient<ds_kloggerd_msgs::KloggerdCmdAction>::GoalHandle goal_handle);

  ds_nav_msgs::AggregatedState state_;

  // UDP connection to MC
  boost::shared_ptr<ds_asio::DsConnection> mc_conn_;

  // UDP connection to act as a ROS sim time server for MC
  boost::shared_ptr<ds_asio::DsConnection> time_conn_;

  // ds_param connection
  ds_param::ParamConnection::Ptr conn_;

  // BF parameters
  ds_control::prm_t bf_prm_;

  // BF state (with altitude)
  ds_control_msgs::BottomFollow1D bf_;

  // DF parameters
  ds_control::GoalDepthFollow::ParamSubscriptions_t df_prm_;

  // DF state
  ds_control_msgs::DepthFollow df_;

  // Goal leg params
  // Starting point for the line
  ds_param::DoubleParam::Ptr start_northing_;
  ds_param::DoubleParam::Ptr start_easting_;
  ds_param::DoubleParam::Ptr start_down_;
  // Endpoint for the line
  ds_param::DoubleParam::Ptr end_northing_;
  ds_param::DoubleParam::Ptr end_easting_;
  ds_param::DoubleParam::Ptr end_down_;
  // Gains
  ds_param::DoubleParam::Ptr kappa_;
  // Leg number
  ds_param::IntParam::Ptr leg_number_;

  // Active controller
  ds_param::IntParam::Ptr active_controller_;

  // Active allocation
  ds_param::IntParam::Ptr active_allocation_;

  // Active depth goal
  ds_param::IntParam::Ptr active_depth_goal_;

  // Active joystick
  ds_param::IntParam::Ptr active_joystick_;

  // subscribers
  ros::Subscriber bf_sub_;
  ros::Subscriber jt_sub_;
  ros::Subscriber jt_bd_sub_;
  ros::Subscriber abort_sub_;
  ros::Subscriber battery_sub_;
  ros::Subscriber main_htp_sub_;
  ros::Subscriber bat_htp_sub_;
  ros::Subscriber xr1_sub_;
  ros::Subscriber xr2_sub_;
  ros::Subscriber agg_sub_;

  // publishers
  ros::Publisher joystick_pub_;
  ros::Publisher shift_pub_;
  ros::Publisher mevt_jtros_pub_;
  ros::Publisher reson_jtros_pub_;

  // ROS service clients
  ros::ServiceClient xr1_cmd_srv_;
  ros::ServiceClient xr2_cmd_srv_;
  ros::ServiceClient servo_fwd_cmd_srv_;
  ros::ServiceClient servo_aft_cmd_srv_;
  ros::ServiceClient pwr_cmd_srv_;
  ros::ServiceClient abort_srv_;
  ros::ServiceClient hdg_srv_;
  ros::ServiceClient reset_dvl_srv_;
  ros::ServiceClient init_ballast_srv_;
  ros::ServiceClient timed_joystick_goal_srv_;
  std::array<ros::ServiceClient, 4> thruster_enable_srv_;
  ros::ServiceClient syprid_srv_;
  ros::ServiceClient supr_srv_;

  // actionlib clients
  std::string em2040_system_id_;
  std::unique_ptr<actionlib::ActionClient<ds_kctrl_msgs::KctrlCmdAction>> kctrl_command_action_client_;
  std::list<actionlib::ClientGoalHandle<ds_kctrl_msgs::KctrlCmdAction>> kctrl_command_goal_handles_;

  std::unique_ptr<actionlib::ActionClient<ds_kloggerd_msgs::KloggerdCmdAction>> kloggerd_command_action_client_;
  std::list<actionlib::ClientGoalHandle<ds_kloggerd_msgs::KloggerdCmdAction>> kloggerd_command_goal_handles_;
  
  // Abort state
  bool aborting_;
  bool decktest_;

  // Power addresses
  std::unordered_map<std::string, uint16_t> pwr_add;
};
}

#endif
