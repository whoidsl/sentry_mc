/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_mc/sentry_mc.h"
#include "std_srvs/SetBool.h"
#include "sentry_acomms/DepthFollowerParameterCommand.h"
#include "sentry_acomms/SetActiveDepthGoalCommand.h"
#include "sentry_acomms/serialization.h"

#include <ds_kctrl_msgs/Kssis.h>

namespace sentry_mc
{
  
void SentryMc::kctrlCommandGoalTransitionCallback(actionlib::ActionClient<ds_kctrl_msgs::KctrlCmdAction>::GoalHandle goal_handle) {
  ROS_INFO_STREAM("kctrl command goal transition: " << goal_handle.getCommState().toString());
  if (goal_handle.getCommState() != actionlib::CommState::DONE) {
    return;
  }

  const auto result = goal_handle.getResult();

  if (!result) {
    ROS_ERROR_STREAM("kctrl command goal is DONE but there is no returned result?");
    goal_handle.reset();
    return;
  }

  const auto state = goal_handle.getTerminalState();
  if (state == actionlib::TerminalState::SUCCEEDED) {
    ROS_INFO_STREAM(state.toString() << " kctrl command: '" << result->command << "', to system id: '" << result->system_id << "'");
  }
  else {
    ROS_WARN_STREAM(state.toString() << " kctrl command: '" << result->command << "', to system id: '" << result->system_id << "'");
  }

  kctrl_command_goal_handles_.remove(goal_handle);
  ROS_INFO_STREAM(kctrl_command_goal_handles_.size() << " pending kctrl commands remain.");
}
  
void SentryMc::handleKloggerdCommand(const std::string& str) {
  
  ROS_DEBUG_STREAM("Received new mc kloggerd command: " << str);

  const auto start = str.find("KLOGGER ");
  if (start == str.npos) {
    ROS_WARN_STREAM("Invalid mc kloggerd command: " << str);
    return;
  }

  auto ok = false;
  auto goal = ds_kloggerd_msgs::KloggerdCmdGoal{};

  auto command_str = str.substr(start + strlen("KLOGGER "));
  if (strcasestr(command_str.c_str(), "START") != nullptr) {
    ROS_INFO("parsed kloggerd start recording command.");
    goal.command = ds_kloggerd_msgs::KloggerdCmdGoal::START;
    ok = true;
  }
  else if (strcasestr(command_str.c_str(), "STOP") != nullptr) {
    ROS_INFO("parsed kloggerd stop recording command.");
    goal.command = ds_kloggerd_msgs::KloggerdCmdGoal::STOP;
    ok = true;
  }
  else if (strcasestr(command_str.c_str(), "NEXT") != nullptr) {
    ROS_INFO("parsed kloggerd next line command.");
    goal.command = ds_kloggerd_msgs::KloggerdCmdGoal::NEXT_LINE;
    ok = true;
  }
  else if (strcasestr(command_str.c_str(), "RESET") != nullptr) {
    ROS_INFO("parsed kloggerd reset command.");
    goal.command = ds_kloggerd_msgs::KloggerdCmdGoal::RESET;
    ok = true;
  }

  if (!ok) {
    ROS_WARN_STREAM("Invalid mc kloggerd command: " << str);
    return;
  }

  kloggerd_command_goal_handles_.push_back(kloggerd_command_action_client_->sendGoal(goal, std::bind(&SentryMc::kloggerdCommandGoalTransitionCallback, this, std::placeholders::_1)));
  ROS_INFO_STREAM(kloggerd_command_goal_handles_.size() << " pending kloggerd commands remain");
}

void SentryMc::kloggerdCommandGoalTransitionCallback(actionlib::ActionClient<ds_kloggerd_msgs::KloggerdCmdAction>::GoalHandle goal_handle) {
  
  ROS_INFO_STREAM("kloggerd command goal transition: " << goal_handle.getCommState().toString());
  if (goal_handle.getCommState() != actionlib::CommState::DONE) {
    return;
  }

  const auto result = goal_handle.getResult();

  if (!result) {
    ROS_ERROR_STREAM("kloggerd command goal is DONE but there is no returned result?");
    goal_handle.reset();
    return;
  }

  const auto state = goal_handle.getTerminalState();
  if (state == actionlib::TerminalState::SUCCEEDED) {
    ROS_INFO_STREAM(state.toString() << " kloggerd command succeeded");
  }
  else {
    ROS_WARN_STREAM(state.toString() << " kloggerd command failed");
  }

  ROS_DEBUG("kloggerd command goal handle reset");
  kloggerd_command_goal_handles_.remove(goal_handle);
  ROS_INFO_STREAM(kloggerd_command_goal_handles_.size() << " pending kloggerd commands remain");
}

SentryMc::~SentryMc() = default;

SentryMc::SentryMc(int argc, char* argv[], const std::string& name) : DsProcess(argc, argv, name)
{
  aborting_ = false;
}

void SentryMc::handleKctrlCommand(const std::string& str) {

  ROS_DEBUG_STREAM("Received new mc kctrl command: " << str);

  auto ok = false;
  auto goal = ds_kctrl_msgs::KctrlCmdGoal{};
  
  if (str.find("KM_PING ") != str.npos) {
    const auto start = str.find("KM_PING ");
    int enable = 0;
    if (std::sscanf(str.c_str() + start, "KM_PING %d", &enable) < 1) {
      ROS_WARN_STREAM("Unable to parse KM_PING command: '" << str << "'.  Expected an integer, found none");
      return;
    }

    if(enable != 0) {
      goal.command = ds_kctrl_msgs::Kssis::KSSIS_PING_START_REQUEST;
    }
    else {
      goal.command = ds_kctrl_msgs::Kssis::KSSIS_PING_STOP_REQUEST;
    }
    goal.system_id = em2040_system_id_;
    ok = true;
  }
  else if (str.find("KM_WC ") != str.npos) {
    const auto start = str.find("KM_WC ");
    int enable = 0;
    if (std::sscanf(str.c_str() + start, "KM_WC %d", &enable) < 1) {
      ROS_WARN_STREAM("Unable to parse KM_WC command: '" << str << "'.  Expected an integer, found none");
      return;
    }

    if(enable != 0) {
      goal.command = ds_kctrl_msgs::Kssis::KSSIS_WATERCOLUMN_START_REQUEST;
    }
    else {
      goal.command = ds_kctrl_msgs::Kssis::KSSIS_WATERCOLUMN_STOP_REQUEST;
    }
    goal.system_id = em2040_system_id_;
    ok = true;
  }
  else if (str.find("KM_PARAM ") != str.npos) {
    const auto start = str.find("KM_PARAM ");
    const auto param_string_size = str.size() - strlen("KM_PARAM ") - start;
    if (param_string_size > 0) {
      goal.command = ds_kctrl_msgs::Kssis::KSSIS_SET_RUNTIME_PARAMS_REQUEST;
      goal.system_id = em2040_system_id_;
      goal.parameters.assign(str.c_str() + start + strlen("KM_PARAM "));
      ok = true;
    }
  }
  else if (str.find("KM_KCTRL ") == 0) {
    ROS_WARN_STREAM("'KM_KCTRL' MC command no longer supported");
  }
  else {
    ROS_WARN_STREAM("Unknown KM command: " << str);
  }

  if (!ok) {
    return;
  }

  kctrl_command_goal_handles_.push_back(kctrl_command_action_client_->sendGoal(goal, std::bind(&SentryMc::kctrlCommandGoalTransitionCallback, this, std::placeholders::_1)));
  ROS_INFO_STREAM(kctrl_command_goal_handles_.size() << " pending kctrl commands remain.");
}

// This provides a common path for both mc and jtros to translate jt commands to ros-land
void SentryMc::processCommandData(std::string mcData)
{
  if (!mcData.find("WLEG"))
  {
    double dbl[6];
    int status;
    ROS_ERROR_STREAM("WLEG received: " << mcData);
    status =
        sscanf(mcData.c_str(), "%*s %lf %lf %lf %lf %lf %lf", &dbl[0], &dbl[1], &dbl[2], &dbl[3], &dbl[4], &dbl[5]);
    if (status == 6)
    {
      // Valid WLEG string, set atomically
      ds_param::ParamGuard lock(conn_);
      start_easting_->Set(dbl[3]);
      start_northing_->Set(dbl[4]);
      start_down_->Set(dbl[5]);
      end_easting_->Set(dbl[0]);
      end_northing_->Set(dbl[1]);
      end_down_->Set(dbl[2]);
    }
    else
      ROS_ERROR_STREAM("Incorrect WLEG received, parsed args: " << status << ", expected 6");
  }
  else if (!mcData.find("WBFP "))
  {
    int param_index;
    double value;
    const auto status = sscanf(mcData.c_str(), "%*s %d %lf", &param_index, &value);
    if (status != 2)
    {
      ROS_ERROR_STREAM("Incorrect WBFP received, parsed args: " << status << ", expected 2");
    }
    else if (param_index < 0 || param_index > 8)
    {
      ROS_ERROR_STREAM("Invalid WBFP index: " << param_index << ", expected between 0 and 8.");
    }
    else
    {
      switch (param_index)
      {
        case 0:
          bf_prm_.alt_d->Set(value);
          break;
        case 1:
          bf_prm_.env->Set(value);
          break;
        case 2:
          bf_prm_.speed_d->Set(value);
          break;
        case 3:
          bf_prm_.speed_gain->Set(value);
          break;
        case 4:
          bf_prm_.depth_rate_d->Set(value);
          break;
        case 5:
          bf_prm_.depth_accel_d->Set(value);
          break;
        case 6:
          bf_prm_.speed_min->Set(value);
          break;
        case 7:
          bf_prm_.alarm_timeout->Set(value);
          break;
        case 8:
          bf_prm_.depth_floor->Set(value);
          break;
        default:
          ROS_ERROR_STREAM("Unexpected WBFP invalid index: " << param_index << " passed guards.  THIS IS A BUG!!!");
          break;
      }
    }
  }
  else if (!mcData.find("WBF"))
  {
    double dbl[9];
    int status;

    status = sscanf(mcData.c_str(), "%*s %lf %lf %lf %lf %lf %lf %lf %lf %lf", &dbl[0], &dbl[1], &dbl[2], &dbl[3],
                    &dbl[4], &dbl[5], &dbl[6], &dbl[7], &dbl[8]);
    if (status == 9)
    {
      // Valid WBF string, set atomically
      ds_param::ParamGuard lock(conn_);
      bf_prm_.alt_d->Set(dbl[0]);
      bf_prm_.env->Set(dbl[1]);
      bf_prm_.speed_d->Set(dbl[2]);
      bf_prm_.speed_gain->Set(dbl[3]);
      bf_prm_.depth_rate_d->Set(dbl[4]);
      bf_prm_.depth_accel_d->Set(dbl[5]);
      bf_prm_.speed_min->Set(dbl[6]);
      bf_prm_.alarm_timeout->Set(dbl[7]);
      bf_prm_.depth_floor->Set(dbl[8]);
    }
    else
      ROS_ERROR_STREAM("Incorrect WBF received, parsed args: " << status << ", expected 9");
  }

  else if (!mcData.find("ROS_ABORT"))
  {
    if (!aborting_)
    {
      aborting_ = true;
      runAbortSequence();
    }
  }
  else if (!mcData.find("ROS_DESCEND"))
  {
    active_controller_->Set(sentry_msgs::SentryControllerEnum::JOYSTICK);
    active_allocation_->Set(sentry_msgs::SentryAllocationEnum::VERTICAL_MODE);
  }
  else if (!mcData.find("ROS_RELEASE_DESCENT"))
  {
    sentry_msgs::XRCmd xr_cmd;
    // Release descent weight (motor 1 on XR2)
    xr_cmd.request.command = xr_cmd.request.XR_CMD_DCAM_OPEN_1;
    serviceRetry(xr2_cmd_srv_, xr_cmd, 10, "XR2 SEND RELEASE DESCENT SUCCESS", "XR2 SEND RELEASE DESCENT FAILURE");
  }
  else if (!mcData.find("ROS_HOLD_DESCENT"))
  {
    sentry_msgs::XRCmd xr_cmd;
    // Hold descent weight (motor 1 on XR2)
    xr_cmd.request.command = xr_cmd.request.XR_CMD_DCAM_CLOSE_1;
    serviceRetry(xr2_cmd_srv_, xr_cmd, 10, "XR2 SEND HOLD DESCENT SUCCESS", "XR2 SEND HOLD DESCENT FAILURE");
  }
  else if (!mcData.find("ROS_BURN_DESCENT"))
  {
    sentry_msgs::XRCmd xr_cmd;
    // Burn descent burnwire (burnwire 1 on XR1(XR1-N1))
    xr_cmd.request.command = xr_cmd.request.XR_CMD_BURNWIRE_ON_1;
    serviceRetry(xr1_cmd_srv_, xr_cmd, 10, "XR1 SEND BURN DESCENT SUCCESS", "XR2 SEND BURN DESCENT FAILURE");
  }
  else if (!mcData.find("MODE SURVEY"))
  {
    ROS_ERROR_STREAM("Enabling survey controller");
    active_controller_->Set(sentry_msgs::SentryControllerEnum::SURVEY);
  }
  else if (!mcData.find("MODE JOYSTICK"))
  {
    ROS_ERROR_STREAM("Enabling joystick controller");
    active_controller_->Set(sentry_msgs::SentryControllerEnum::JOYSTICK);
  }
  else if (!mcData.find("ROS_JOYSTICK_MC"))
  {
    ROS_ERROR_STREAM("Enabling mc joystick");
    active_joystick_->Set(ds_control_msgs::JoystickEnum::MC);
  }
  else if (!mcData.find("ROS_JOYSTICK_CONJOY"))
  {
    ROS_ERROR_STREAM("Enabling joy joystick");
    active_joystick_->Set(sentry_msgs::SentryJoystickEnum::CONJOY);
  }
  else if (!mcData.find("ROS_JOYSTICK_ACOUSTIC"))
  {
    ROS_ERROR_STREAM("Enabling acoustic joystick");
    active_joystick_->Set(sentry_msgs::SentryJoystickEnum::TIMED);
  }
  else if (!mcData.find("SJY ADC"))
  {
    // Make a joystick message
    ds_nav_msgs::AggregatedState joystick_ref;
    double dbl[8];
    int status;

    status = sscanf(mcData.c_str(), "SJY ADC %lf %lf %lf %lf %lf %lf %lf %lf", &dbl[0], &dbl[1], &dbl[2], &dbl[3],
                    &dbl[4], &dbl[5], &dbl[6], &dbl[7]);
    if (status == 8)
    {
      joystick_ref.surge_u.value = dbl[0];
      joystick_ref.surge_u.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
      joystick_ref.r.value = dbl[3];
      joystick_ref.r.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
      joystick_ref.heave_w.value = dbl[2];
      joystick_ref.heave_w.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;

      joystick_pub_.publish(joystick_ref);
    }
    else
      ROS_ERROR_STREAM("Incorrect SJY ADC received, parsed args: " << status << ", expected 8");
  }
  else if (!mcData.find("SPON"))
  {
    unsigned int address;
    int status;
    status = sscanf(mcData.c_str(), "SPON %x", &address);
    if (status == 1)
    {
      sentry_msgs::PWRCmd pwr_cmd;
      pwr_cmd.request.address = address;
      pwr_cmd.request.command = pwr_cmd.request.PWR_CMD_ON;
      serviceRetry(pwr_cmd_srv_, pwr_cmd, 10, "SPON succeeded", "SPON failed");
    }
  }
  else if (!mcData.find("SPOFF"))
  {
    unsigned int address;
    int status;
    status = sscanf(mcData.c_str(), "SPOFF %x", &address);
    if (status == 1)
    {
      sentry_msgs::PWRCmd pwr_cmd;
      pwr_cmd.request.address = address;
      pwr_cmd.request.command = pwr_cmd.request.PWR_CMD_OFF;
      serviceRetry(pwr_cmd_srv_, pwr_cmd, 10, "SPON succeeded", "SPON failed");
    }
  }
  else if (!mcData.find("WMD"))
  {
    // Determines the allocation
    int alloc;
    int status;
    status = sscanf(mcData.c_str(), "WMD %d", &alloc);
    if ((alloc == 0) || (alloc == 1))
    {
      // Flight mode
      active_allocation_->Set(sentry_msgs::SentryAllocationEnum::FLIGHT_MODE);
    }
    else if (alloc == 6)
    {
      // Vertical mode
      active_allocation_->Set(sentry_msgs::SentryAllocationEnum::VERTICAL_MODE);
    }
  }
  else if (!mcData.find("ENC CON_BUS_SAIL_THREAD CMD_SERVO8_SLEEP 1"))
  {
    sentry_msgs::SailServoCmd scmd;
    scmd.request.command = scmd.request.SERVO_CMD_SLEEP;
    serviceRetry(servo_fwd_cmd_srv_, scmd, 10, "SERVO_CMD_SLEEP success", "SERVO_CMD_SLEEP failure");
  }
  else if (!mcData.find("ENC CON_BUS_SAIL_THREAD CMD_SERVO2_SLEEP 1"))
  {
    sentry_msgs::SailServoCmd scmd;
    scmd.request.command = scmd.request.SERVO_CMD_SLEEP;
    serviceRetry(servo_aft_cmd_srv_, scmd, 10, "SERVO_CMD_SLEEP success", "SERVO_CMD_SLEEP failure");
  }
  else if (!mcData.find("ENC CON_BUS_SAIL_THREAD CMD_SERVO8_INIT 1"))
  {
    sentry_msgs::SailServoCmd scmd;
    scmd.request.command = scmd.request.SERVO_CMD_INDEX;
    serviceRetry(servo_fwd_cmd_srv_, scmd, 10, "SERVO_CMD_INDEX success", "SERVO_CMD_INDEX failure");
  }
  else if (!mcData.find("ENC CON_BUS_SAIL_THREAD CMD_SERVO2_INIT 1"))
  {
    sentry_msgs::SailServoCmd scmd;
    scmd.request.command = scmd.request.SERVO_CMD_INDEX;
    serviceRetry(servo_aft_cmd_srv_, scmd, 10, "SERVO_CMD_INDEX success", "SERVO_CMD_INDEX failure");
  }
  else if (!mcData.find("INDEX FWD"))
  {
    sentry_msgs::SailServoCmd scmd;
    scmd.request.command = scmd.request.SERVO_CMD_SLEEP;
    serviceRetry(servo_fwd_cmd_srv_, scmd, 10, "SERVO_CMD_SLEEP success", "SERVO_CMD_SLEEP failure");
    sentry_msgs::SailServoCmd scmd2;
    scmd2.request.command = scmd.request.SERVO_CMD_INDEX;
    serviceRetry(servo_fwd_cmd_srv_, scmd2, 10, "SERVO_CMD_INDEX success", "SERVO_CMD_INDEX failure");
  }
  else if (!mcData.find("INDEX AFT"))
  {
    sentry_msgs::SailServoCmd scmd;
    scmd.request.command = scmd.request.SERVO_CMD_SLEEP;
    serviceRetry(servo_aft_cmd_srv_, scmd, 10, "SERVO_CMD_SLEEP success", "SERVO_CMD_SLEEP failure");
    sentry_msgs::SailServoCmd scmd2;
    scmd2.request.command = scmd.request.SERVO_CMD_INDEX;
    serviceRetry(servo_aft_cmd_srv_, scmd2, 10, "SERVO_CMD_INDEX success", "SERVO_CMD_INDEX failure");
  }
  else if (!mcData.find("THRUSTERS_DISABLE"))
  {
    std_srvs::SetBool scmd;
    scmd.request.data = false;
    serviceRetry(thruster_enable_srv_[0], scmd, 10, "THRUSTERS_DISABLE[0] SUCCESS", "THRUSTERS_DISABLE[0] FAILURE");
    scmd.request.data = false;
    serviceRetry(thruster_enable_srv_[1], scmd, 10, "THRUSTERS_DISABLE[1] SUCCESS", "THRUSTERS_DISABLE[1] FAILURE");
    scmd.request.data = false;
    serviceRetry(thruster_enable_srv_[2], scmd, 10, "THRUSTERS_DISABLE[2] SUCCESS", "THRUSTERS_DISABLE[2] FAILURE");
    scmd.request.data = false;
    serviceRetry(thruster_enable_srv_[3], scmd, 10, "THRUSTERS_DISABLE[3] SUCCESS", "THRUSTERS_DISABLE[3] FAILURE");
  }
  else if (!mcData.find("THRUSTERS_ENABLE"))
  {
    std_srvs::SetBool scmd;
    scmd.request.data = true;
    serviceRetry(thruster_enable_srv_[0], scmd, 10, "THRUSTERS_DISABLE[0] SUCCESS", "THRUSTERS_DISABLE[0] FAILURE");
    scmd.request.data = true;
    serviceRetry(thruster_enable_srv_[1], scmd, 10, "THRUSTERS_DISABLE[1] SUCCESS", "THRUSTERS_DISABLE[1] FAILURE");
    scmd.request.data = true;
    serviceRetry(thruster_enable_srv_[2], scmd, 10, "THRUSTERS_DISABLE[2] SUCCESS", "THRUSTERS_DISABLE[2] FAILURE");
    scmd.request.data = true;
    serviceRetry(thruster_enable_srv_[3], scmd, 10, "THRUSTERS_DISABLE[3] SUCCESS", "THRUSTERS_DISABLE[3] FAILURE");
  }
  else if (!mcData.find("UTL"))
  {
    ROS_ERROR_STREAM("UTL received: " << mcData);
    int tl = 0;
    int status = 0;
    status = sscanf(mcData.c_str(), "UTL %d", &tl);
    if (status == 1)
    {
      ROS_ERROR_STREAM("UTL parsed, setting leg number to " << tl);
      leg_number_->Set(tl);
    }
  }
  else if (!mcData.find("MCSHFT"))
  {
    double shift_easting = 0;
    double shift_northing = 0;
    int status = 0;
    // ROS_ERROR_STREAM("Shift");
    status = sscanf(mcData.c_str(), "MCSHFT %lf %lf", &shift_easting, &shift_northing);
    if (status == 2)
    {
      ds_nav_msgs::Shift out{};
      ros::Time now = ros::Time::now();
      out.header.stamp = now;
      out.shift_easting = shift_easting;
      out.shift_northing = shift_northing;
      shift_pub_.publish(out);
    }
  }
  else if (!mcData.find("RESETDVL"))
  {
    double reset_easting = 0;
    double reset_northing = 0;
    int status = 0;
    status = sscanf(mcData.c_str(), "RESETDVL %lf %lf", &reset_easting, &reset_northing);
    if (status == 2)
    {
      ds_nav_msgs::ResetDvl reset_cmd;
      reset_cmd.request.reset_easting = reset_easting;
      reset_cmd.request.reset_northing = reset_northing;
      serviceRetry(reset_dvl_srv_, reset_cmd, 10, "Dvl reset", "Failed to reset dvl");
    }
  }
  else if (!mcData.find("AUTOHDG"))
  {
    ROS_ERROR_STREAM("AUTOHDG received: " << mcData);
    int autohdg = 0;
    int status = 0;
    status = sscanf(mcData.c_str(), "AUTOHDG %d", &autohdg);
    if (status == 1)
    {
      if (autohdg == 0)
      {
        ds_base::BoolCommand hdg_cmd;
        hdg_cmd.request.command = false;
        serviceRetry(hdg_srv_, hdg_cmd, 10, "Autoheading disabled", "Autoheading disable service call failed");
      }
      if (autohdg == 1)
      {
        ds_base::BoolCommand hdg_cmd;
        hdg_cmd.request.command = true;
        serviceRetry(hdg_srv_, hdg_cmd, 10, "Autoheading disabled", "Autoheading disable service call failed");
      }
    }
  }
  else if (!mcData.find("MEVT"))
  {
    // Publish on MEVT jtros topic
    ds_core_msgs::StringStamped pstr;
    pstr.header.stamp = ros::Time::now();
    pstr.payload = mcData;
    mevt_jtros_pub_.publish(pstr);
  }
  else if (!mcData.find("RESONMSG"))
  {
    std_msgs::String pstr;
    pstr.data = mcData;
    reson_jtros_pub_.publish(pstr);
  }
  else if (!mcData.find("SDY"))
  {
    ;
  }
  else if (!mcData.find("EXD"))
  {
    ;
  }
  else if (!mcData.find("INIT_BALLAST "))
  {
    uint8_t hold_altitude;
    float testpoint = 0;
    const auto num_scanned = sscanf(mcData.data(), "INIT_BALLAST %hhu %f", &hold_altitude, &testpoint);

    if (num_scanned == 2)
    {
      sentry_msgs::InitiateBallastTest scmd;
      scmd.request.hold_altitude = hold_altitude;
      scmd.request.ballast_testpoint = testpoint;
      serviceRetry(init_ballast_srv_, scmd, 10, "INIT_BALLAST_TEST success", "INIT_BALLAST_TEST failure");
    }
  }

  else if (!mcData.find("ACOUSTIC_JOYSTICK "))
  {
    uint8_t hour = 0;
    uint8_t min = 0;
    uint8_t seconds = 0;

    float surge_u = 0;
    float heave_w = 0;
    float torque_w = 0;

    uint8_t allocation = 0;

    const auto num_scanned = sscanf(mcData.data(), "ACOUSTIC_JOYSTICK %hhu:%hhu:%hhu %hhu %f %f %f", &hour, &min,
                                    &seconds, &allocation, &surge_u, &heave_w, &torque_w);

    if (num_scanned == 7)
    {
      sentry_msgs::TimedJoystickService scmd;
      scmd.request.joystick.timeout.fromSec(3600 * hour + 60 * min + seconds);
      scmd.request.joystick.controller = sentry_msgs::SentryControllerEnum::JOYSTICK;
      scmd.request.joystick.allocation = allocation;

      scmd.request.joystick.state.surge_u.value = surge_u;
      scmd.request.joystick.state.surge_u.valid = true;

      scmd.request.joystick.state.heave_w.value = heave_w;
      scmd.request.joystick.state.heave_w.valid = true;

      scmd.request.joystick.state.r.value = torque_w;
      scmd.request.joystick.state.r.valid = true;

      // Switch the active joystick to the timed joystick
      // active_joystick_->Set(sentry_msgs::SentryJoystickEnum::TIMED);

      // Send the command
      serviceRetry(timed_joystick_goal_srv_, scmd, 10, "ACOUSTIC_JOYSTICK success", "ACOUSTIC_JOYSTICK failure");
    }
  }
  else if (mcData.find("KM_") == 0)
  {
    handleKctrlCommand(mcData);
  }
  else if ((mcData.find("KLOGGER ") == 0)) {
    handleKloggerdCommand(mcData);
  }
  else if (!mcData.find("PKZ ")) {
    auto srv = sentry_msgs::SypridCmd{};
    if (!mcData.find("PKZ START ")){
      double t1 = 0;
      double t2 = 0;
      auto num_scanned = sscanf(mcData.data(), "PKZ START ALL %lf %lf", &t1, &t2);
      if (num_scanned == 2){
        srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_START_ALL;
        srv.request.torque_1 = t1;
        srv.request.torque_2 = t2;
      }
      num_scanned = sscanf(mcData.data(), "PKZ START PORT %lf", &t1);
      if (num_scanned == 1){
        srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_START_PORT;
        srv.request.torque_1 = t1;
      }
      num_scanned = sscanf(mcData.data(), "PKZ START STBD %lf", &t1);
      if (num_scanned == 1){
        srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_START_STBD;
        srv.request.torque_1 = t1;
      }
    } else if (!mcData.find("PKZ STOP ALL")){
      srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_STOP_ALL;
    } else if (!mcData.find("PKZ STOP PORT")){
      srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_STOP_PORT;
    } else if (!mcData.find("PKZ STOP STBD")){
      srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_STOP_STBD;
    } else if (!mcData.find("PKZ OPEN PORT")){
      srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_OPEN_DCAM_PORT;
    } else if (!mcData.find("PKZ OPEN STBD")){
      srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_OPEN_DCAM_STBD;
    } else if (!mcData.find("PKZ CLOSE PORT")){
      srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_CLOSE_DCAM_PORT;
    } else if (!mcData.find("PKZ CLOSE STBD")){
      srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_CLOSE_DCAM_STBD;
    } else if (!mcData.find("PKZ ENABLE DCAM PERSISTENCE")){
      srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_ENABLE_DCAM_PERSISTENCE;
    } else if (!mcData.find("PKZ DISABLE DCAM PERSISTENCE")){
      srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_DISABLE_DCAM_PERSISTENCE;
    } else if (!mcData.find("PKZ REQUEST FLOW SENSOR DATA")){
      srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_REQUEST_FLOW_SENSOR_DATA;
    } else if (!mcData.find("PKZ TORQUE ")){
      double t = 0;
      auto num_scanned = sscanf(mcData.data(), "PKZ TORQUE PORT %lf", &t);
      if (num_scanned == 1){
        srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_TORQUE_PORT;
        srv.request.torque_1 = t;
      }
      num_scanned = sscanf(mcData.data(), "PKZ TORQUE STBD %lf", &t);
      if (num_scanned == 1){
        srv.request.pkz_cmd = sentry_msgs::SypridCmd::Request::PKZ_TORQUE_STBD;
        srv.request.torque_1 = t;
      }
    }
    if (srv.request.pkz_cmd != 0 && syprid_srv_.exists()){
      if (syprid_srv_.call(srv.request, srv.response))
      {
        ROS_ERROR_STREAM("Syprid command service called successfully");
        ROS_ERROR_STREAM("SypridCmd Srv Resp: "<<srv.response.action);
      } else {
        ROS_ERROR_STREAM("Syprid command service call failed");
      }
    } else {
      ROS_ERROR_STREAM("Syprid command service on " << syprid_srv_.getService() << "doesn't exist");
    }
  }

  else if (!mcData.find("SUPR "))
  {
    if (!mcData.find("SUPR SAMPLE"))
    {
      auto srv = ds_supr::SuprCmd{};
      srv.request.supr_cmd = ds_supr::SuprCmd::Request::START_SAMPLE;

      if (srv.request.supr_cmd != 0 && supr_srv_.exists())
      {
        if (supr_srv_.call(srv.request, srv.response))
        {
          ROS_ERROR_STREAM("Supr command service called successfully");
          ROS_ERROR_STREAM("SuprCmd Srv Resp: " << srv.response.action);
        }
        else
        {
          ROS_ERROR_STREAM("Supr command service call failed");
        }
      }
      else
      {
        ROS_ERROR_STREAM("Supr command service on " << supr_srv_.getService() << "doesn't exist");
      }
    }
  }
  else if (!mcData.find("WDFP "))
  {
    auto wdfp = sentry_acomms::DepthFollowerParameterCommand{};
    const auto success = ds_acomms_serialization::deserialize(mcData, wdfp);

    if(!success)
    {
      if (wdfp.index == sentry_acomms::DepthFollowerParameterCommand::INDEX_INVALID)
      {
        ROS_ERROR_STREAM("Invalid WDFP index in string: " << mcData << ", expected between " << static_cast<int>(sentry_acomms::DepthFollowerParameterCommand::INDEX_MIN) << " and " << static_cast<int>(sentry_acomms::DepthFollowerParameterCommand::INDEX_MAX));
      }
      else {
        ROS_ERROR_STREAM("Failed to parse WDFP msg: ' " << mcData << "'");
      }
      return;
    }

    switch (wdfp.index)
    {
      case sentry_acomms::DepthFollowerParameterCommand::INDEX_DEPTH:
        df_prm_.depth->Set(wdfp.value);
        break;
      case sentry_acomms::DepthFollowerParameterCommand::INDEX_ENVELOPE:
        df_prm_.envelope->Set(wdfp.value);
        break;
      case sentry_acomms::DepthFollowerParameterCommand::INDEX_SPEED:
        df_prm_.speed->Set(wdfp.value);
        break;
      case sentry_acomms::DepthFollowerParameterCommand::INDEX_SPEED_GAIN:
        df_prm_.speed_gain->Set(wdfp.value);
        break;
      case sentry_acomms::DepthFollowerParameterCommand::INDEX_DEPTH_RATE:
        df_prm_.depth_rate->Set(wdfp.value);
        break;
      case sentry_acomms::DepthFollowerParameterCommand::INDEX_DEPTH_ACCELERATION:
        df_prm_.depth_acceleration->Set(wdfp.value);
        break;
      case sentry_acomms::DepthFollowerParameterCommand::INDEX_MINIMUM_SPEED:
        df_prm_.minimum_speed->Set(wdfp.value);
        break;
      case sentry_acomms::DepthFollowerParameterCommand::INDEX_MINIMUM_ALTITUDE:
        df_prm_.minimum_altitude->Set(wdfp.value);
        break;
      case sentry_acomms::DepthFollowerParameterCommand::INDEX_DEPTH_FLOOR:
        df_prm_.depth_floor->Set(wdfp.value);
        break;
      default:
        ROS_ERROR_STREAM("Unexpected WDFP invalid index: " << wdfp.index << " passed guards.  THIS IS A BUG!!!");
        break;
    }
  }
  else if (!mcData.find("SADG "))
  {
    auto sadg = sentry_acomms::SetActiveDepthGoalCommand{};
    const auto success = ds_acomms_serialization::deserialize(mcData, sadg);

    if(!success)
    {
      ROS_ERROR_STREAM("Failed to parse SADG msg: ' " << mcData << "'");
      return;
    }

    switch (sadg.goal)
    {
      case sentry_acomms::SetActiveDepthGoalCommand::INDEX_CONSTANT_ALTITUDE:
        ROS_INFO_STREAM("Setting active goal depth to: " << sadg.goal << " (CONSTANT_ALTITUDE)");
        active_depth_goal_->Set(sadg.goal);
        break;
      case sentry_acomms::SetActiveDepthGoalCommand::INDEX_CONSTANT_DEPTH:
        ROS_INFO_STREAM("Setting active goal depth to: " << sadg.goal << " (CONSTANT_DEPTH)");
        active_depth_goal_->Set(sadg.goal);
        break;
      default:
        ROS_WARN_STREAM("Unknown goal depth: " << sadg.goal << ", taking no action");
        break;
    }
  }

  else
  {
    ROS_ERROR_STREAM("Received message not supported: " << mcData);
  }
}

void SentryMc::onMcData(ds_core_msgs::RawData bytes)
{
  std::string mcData(bytes.data.begin(), bytes.data.end());

  processCommandData(mcData);
}

void SentryMc::onTimeData(ds_core_msgs::RawData bytes)
{
  std::string mcData(bytes.data.begin(), bytes.data.end());

  // Request for current time
  if (!mcData.find("GCT"))
  {
    double now_time = bytes.ds_header.io_time.sec + bytes.ds_header.io_time.nsec / 1000000000.0;
    std::stringstream time;
    time << std::fixed << "TTI " << now_time;
    time_conn_->send(time.str());
  }
}

void SentryMc::onJtRosMsg(const std_msgs::String::ConstPtr msg)
{
  std::string data = msg->data;
  std::string out;
  bool mca_found = false;

  ROS_DEBUG_STREAM("onJtRosMsg handler received: " << *msg);
  const auto equals_idx = data.find_first_of(' ');
  if (std::string::npos != equals_idx)
  {
    out = data.substr(equals_idx + 1);
    ROS_DEBUG_STREAM("msg remainder after first space: '" << out << "'");
    if (!data.find("MCA"))
    {
      mca_found = true;
      ROS_INFO_STREAM("sending to MC: '" << out << "'");
      mc_conn_->send(out);
    }
  }
  else
  {
    ROS_DEBUG_STREAM("no 'space' found in message: '" << out << "'");
  }

  if (!mca_found) {
    ROS_DEBUG_STREAM("'MCA' not found in message, running processCommandData callback");
    processCommandData(data);
  }
}

template <typename T>
bool SentryMc::serviceRetry(ros::ServiceClient srvClient, T srv, int n_retries, std::string success,
                            std::string failure)
{
  for (int i = 0; i < n_retries; ++i)
  {
    if (srvClient.call(srv))
    {
      ROS_INFO_STREAM(success);
      return true;
    }
    else
    {
      ROS_ERROR_STREAM(failure);
    }
  }
  return false;
}

void SentryMc::onAbortMsg(const ds_core_msgs::Abort::ConstPtr msg)
{
  if (msg->enable)
  {
    if (msg->abort)
    {
      std::stringstream abort;
      abort << "XRD 1 5";
      mc_conn_->send(abort.str());
    }
  }
}

void SentryMc::runAbortSequence(void)
{
  active_controller_->Set(sentry_msgs::SentryControllerEnum::JOYSTICK);
  active_allocation_->Set(sentry_msgs::SentryAllocationEnum::VERTICAL_MODE);

  // Tell the abort supervisor that we are aborting
  ds_hotel_msgs::AbortCmd abort_cmd;
  abort_cmd.request.command = abort_cmd.request.ABORT_CMD_ENABLE;
  serviceRetry(abort_srv_, abort_cmd, 10, "Abort enabled", "Abort enable service call failed");
  abort_cmd.request.command = abort_cmd.request.ABORT_CMD_ABORT;
  serviceRetry(abort_srv_, abort_cmd, 10, "Aborting", "Abort service call failed");

  ds_nav_msgs::AggregatedState joystick_ref;
  joystick_ref.surge_u.value = 0.0;
  joystick_ref.surge_u.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
  joystick_ref.r.value = 0.0;
  joystick_ref.r.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
  joystick_ref.heave_w.value = 0.0;
  joystick_ref.heave_w.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
  joystick_pub_.publish(joystick_ref);
}

void SentryMc::onAggregatedStateMsg(const ds_nav_msgs::AggregatedState::ConstPtr msg)
{
  state_ = *msg;

  boost::posix_time::ptime now_ptime = msg->header.stamp.toBoost();
  boost::posix_time::time_facet* facet = new boost::posix_time::time_facet("%Y/%m/%d %H:%M:%S.%f");
  // Send AUV message to MC
  std::stringstream auv;
  auv.imbue(std::locale(auv.getloc(), facet));
  auv << "AUV ";
  auv << now_ptime << " ";
  auv << "SEN ";
  auv << std::fixed << state_.easting.value << " ";
  auv << state_.northing.value << " ";
  auv << state_.down.value << " ";
  auv << state_.heading.value << " ";
  auv << state_.pitch.value << " ";
  auv << state_.roll.value << " ";
  auv << state_.down.value << " ";
  auv << "0.0 ";  // lframe_pos[0]
  auv << "0.0 ";  // lframe_pos[1]
  auv << bf_.raw_altitude << " ";
  auv << state_.surge_u.value << " ";
  auv << state_.sway_v.value << " ";
  auv << state_.heave_w.value << " ";
  auv << state_.r.value << " ";
  auv << state_.q.value << " ";
  auv << state_.p.value << " ";
  auv << "1500.0 ";
  mc_conn_->send(auv.str());

  // If in decktest mode, send a leg that tracks the heading
  if (decktest_)
  {
    ds_param::ParamGuard lock(conn_);
    start_easting_->Set(state_.easting.value);
    start_northing_->Set(state_.northing.value);
    start_down_->Set(state_.down.value);
    double end_easting = state_.easting.value + 10 * sin(state_.heading.value);
    double end_northing = state_.northing.value + 10 * cos(state_.heading.value);
    end_easting_->Set(end_easting);
    end_northing_->Set(end_northing);
    end_down_->Set(state_.down.value);
    // ROS_ERROR_STREAM(" Setting leg: " << state_.easting.value << " " << state_.northing.value << " " << end_easting
    // << " " << end_northing << " ");
  }
}

void SentryMc::onBottomFollowMsg(const ds_control_msgs::BottomFollow1D::ConstPtr msg)
{
  bf_ = *msg;
}

void SentryMc::onBatmanMsg(const ds_hotel_msgs::BatMan::ConstPtr msg)
{
  // GAS 2017/02/11 08:00:11.364 TOTAL 877648.875000 66.579682 BAT00 66 0.000000 0.000000 0.0000 0.0 0 0 0 0 BAT01 0
  // 179624.750000 66.935184 52.7100 12.0 1 0 0 0 BAT02 0 170300.750000 65.732977 52.6400 14.0 1 0 0 0 BAT03 0
  // 173641.000000 66.221291 52.8500 13.0 1 0 0 0 BAT04 0 181867.250000 67.322709 52.5700 13.0 1 0 0 0 BAT05 66 0.000000
  // 0.000000 0.0000 0.0 0 0 0 0 BAT06 0 172215.125000 66.646301 52.2300 11.0 1 0 0 0 BAT07 66 0.000000 0.000000 0.0000
  // 0.0 0 0 0 0 BAT08 66 0.000000 0.000000 0.0000 0.0 0 0 0 0 BAT09 66 0.000000 0.000000 0.0000 0.0 0 0 0 0 BAT10 66
  // 0.000000 0.000000 0.0000 0.0 0 0 0 0
  // However, only this is needed by the MC so we send this truncated version given that BatMan.msg does not contain
  // other stats for now:
  // GAS 2017/02/11 08:00:11.364 TOTAL 877648.875000 66.579682 BAT00

  boost::posix_time::ptime now_ptime = msg->header.stamp.toBoost();
  boost::posix_time::time_facet* facet = new boost::posix_time::time_facet("%Y/%m/%d %H:%M:%S.%f");
  // Send truncated GAS message to MC
  std::stringstream gas;
  gas.imbue(std::locale(gas.getloc(), facet));
  gas << std::fixed << "GAS ";
  gas << now_ptime << " ";
  gas << "TOTAL ";
  gas << msg->chargeCoulombs << " ";
  gas << msg->percentFull << " ";
  gas << "BAT00";
  mc_conn_->send(gas.str());

  // Use new BVOLTS message that I implemented in MC to use min and max volts - the previous GRD message was broken
  std::stringstream bvolts;
  bvolts << std::fixed << "BVOLTS ";
  bvolts << msg->minModuleVolt << " ";
  bvolts << msg->maxModuleVolt;
  mc_conn_->send(bvolts.str());
}

void SentryMc::onHtpMsg(const ds_hotel_msgs::HTP::ConstPtr msg, std::string number, std::string num_chars)
{
  //     H  T   P   num_chars
  // HTP2 47 3.7 6.6 26
  // HTP3 9 17.7 17.9 52
  std::stringstream htp;
  htp << std::fixed << "HTP" << number << " ";
  htp << msg->humidity;
  htp << msg->temperature;
  htp << msg->pressure;
  htp << num_chars;
  mc_conn_->send(htp.str());
}

void SentryMc::onXrMsg(const ds_hotel_msgs::XR::ConstPtr msg, std::string number)
{
  std::stringstream xrc;
  xrc << "XRC " << number << " " << msg->C;
  mc_conn_->send(xrc.str());

  std::stringstream xrd;
  xrd << std::fixed << "XRD " << number << " " << msg->deadsecs;
  mc_conn_->send(xrd.str());
}

void SentryMc::setup() {
  DsProcess::setup();
  setupActions();
}

void SentryMc::setupConnections()
{
  ds_base::DsProcess::setupConnections();

  mc_conn_ = addConnection("mc_connection", boost::bind(&SentryMc::onMcData, this, _1));
  time_conn_ = addConnection("time_connection", boost::bind(&SentryMc::onTimeData, this, _1));
}

void SentryMc::setupParameters()
{
  ds_base::DsProcess::setupParameters();

  auto nh = nodeHandle();
  conn_ = ds_param::ParamConnection::create(nh);

  decktest_ = ros::param::param<bool>("~decktest_mode", false);

  std::string bf_ns = ros::param::param<std::string>("~bf_ns", "/jhrov");

  bf_prm_.alt_d = conn_->connect<ds_param::DoubleParam>(bf_ns + "/altitude");
  bf_prm_.env = conn_->connect<ds_param::DoubleParam>(bf_ns + "/envelope");
  bf_prm_.speed_d = conn_->connect<ds_param::DoubleParam>(bf_ns + "/speed");
  bf_prm_.speed_gain = conn_->connect<ds_param::DoubleParam>(bf_ns + "/speed_gain");
  bf_prm_.depth_rate_d = conn_->connect<ds_param::DoubleParam>(bf_ns + "/depth_rate");
  bf_prm_.depth_accel_d = conn_->connect<ds_param::DoubleParam>(bf_ns + "/depth_acceleration");
  bf_prm_.speed_min = conn_->connect<ds_param::DoubleParam>(bf_ns + "/speed_minimum");
  bf_prm_.alarm_timeout = conn_->connect<ds_param::DoubleParam>(bf_ns + "/alarm_timeout");
  bf_prm_.depth_floor = conn_->connect<ds_param::DoubleParam>(bf_ns + "/depth_floor");
  bf_prm_.alt_max_range = conn_->connect<ds_param::DoubleParam>(bf_ns + "/alt_max_range");
  bf_prm_.alt_bad_max_range = conn_->connect<ds_param::DoubleParam>(bf_ns + "/alt_bad_max_range");
  bf_prm_.alt_min_range = conn_->connect<ds_param::DoubleParam>(bf_ns + "/alt_min_range");
  bf_prm_.alt_bad_min_range = conn_->connect<ds_param::DoubleParam>(bf_ns + "/alt_bad_min_range");
  bf_prm_.alt_bad_indeterminant = conn_->connect<ds_param::DoubleParam>(bf_ns + "/alt_bad_indeterminant");
  bf_prm_.alt_bad_timeout = conn_->connect<ds_param::DoubleParam>(bf_ns + "/alt_bad_timeout");
  bf_prm_.speed_outside = conn_->connect<ds_param::DoubleParam>(bf_ns + "/speed_outside");
  bf_prm_.median_tol = conn_->connect<ds_param::DoubleParam>(bf_ns + "/median_tol");
  bf_prm_.time_low_depth_rate_limit = conn_->connect<ds_param::DoubleParam>(bf_ns + "/time_low_depth_rate_limit");
  bf_prm_.depth_rate_alpha = conn_->connect<ds_param::DoubleParam>(bf_ns + "/depth_rate_alpha");
  bf_prm_.depth_rate_threshold = conn_->connect<ds_param::DoubleParam>(bf_ns + "/depth_rate_threshold");
  bf_prm_.alt_fly_down = conn_->connect<ds_param::DoubleParam>(bf_ns + "/alt_fly_down");
  bf_prm_.slow_above_env = conn_->connect<ds_param::BoolParam>(bf_ns + "/slow_above_env");

  std::string df_ns = ros::param::param<std::string>("~df_ns", "/jhrov");

  df_prm_.depth = conn_->connect<ds_param::DoubleParam>(df_ns + "/depth");
  df_prm_.minimum_altitude = conn_->connect<ds_param::DoubleParam>(df_ns + "/minimum_altitude");
  df_prm_.speed = conn_->connect<ds_param::DoubleParam>(df_ns + "/speed");
  df_prm_.speed_gain = conn_->connect<ds_param::DoubleParam>(df_ns + "/speed_gain");
  df_prm_.depth_rate = conn_->connect<ds_param::DoubleParam>(df_ns + "/depth_rate");
  df_prm_.depth_acceleration = conn_->connect<ds_param::DoubleParam>(df_ns + "/depth_acceleration");
  df_prm_.minimum_speed = conn_->connect<ds_param::DoubleParam>(df_ns + "/minimum_speed");
  df_prm_.envelope = conn_->connect<ds_param::DoubleParam>(df_ns + "/envelope");
  df_prm_.depth_floor = conn_->connect<ds_param::DoubleParam>(df_ns + "/depth_floor");

  std::string leg_ns = ros::param::param<std::string>("~leg_ns", "0");

  start_northing_ = conn_->connect<ds_param::DoubleParam>(leg_ns + "/start_northing");
  start_easting_ = conn_->connect<ds_param::DoubleParam>(leg_ns + "/start_easting");
  start_down_ = conn_->connect<ds_param::DoubleParam>(leg_ns + "/start_down");
  end_northing_ = conn_->connect<ds_param::DoubleParam>(leg_ns + "/end_northing");
  end_easting_ = conn_->connect<ds_param::DoubleParam>(leg_ns + "/end_easting");
  end_down_ = conn_->connect<ds_param::DoubleParam>(leg_ns + "/end_down");
  kappa_ = conn_->connect<ds_param::DoubleParam>(leg_ns + "/kappa");
  leg_number_ = conn_->connect<ds_param::IntParam>(leg_ns + "/leg_number");

  std::string controller_ns = ros::param::param<std::string>("~controller_ns", "0");
  std::string goals_ns = ros::param::param<std::string>("~goals_ns", "0");
  active_controller_ = conn_->connect<ds_param::IntParam>(controller_ns + "/active_controller");
  active_allocation_ = conn_->connect<ds_param::IntParam>(controller_ns + "/active_allocation");
  active_depth_goal_ = conn_->connect<ds_param::IntParam>(controller_ns + "/active_depth_goal");
  active_joystick_ = conn_->connect<ds_param::IntParam>(goals_ns + "/active_joystick");

  // The name of the stuff to power on/off should come from param server
  pwr_add["sbp"] = grd_util::get_address("sbp");
  pwr_add["strobe"] = grd_util::get_address("strobe");
  pwr_add["lasers"] = grd_util::get_address("lasers");
  pwr_add["camera"] = grd_util::get_address("camera");
}

void SentryMc::setupSubscriptions()
{
  ds_base::DsProcess::setupSubscriptions();

  std::string agg_topic = ros::param::param<std::string>("~agg_topic", "0");
  agg_sub_ = nodeHandle().subscribe<ds_nav_msgs::AggregatedState>(
      agg_topic, 1, boost::bind(&SentryMc::onAggregatedStateMsg, this, _1));
  std::string jtros_ns = ros::param::param<std::string>("~jtros_ns", "0");
  jt_sub_ = nodeHandle().subscribe<std_msgs::String>(jtros_ns, 1, boost::bind(&SentryMc::onJtRosMsg, this, _1));
  std::string bf_ns = ros::param::param<std::string>("~bf_topic", "0");
  bf_sub_ = nodeHandle().subscribe<ds_control_msgs::BottomFollow1D>(
      bf_ns, 1, boost::bind(&SentryMc::onBottomFollowMsg, this, _1));
  std::string abort_topic = ros::param::param<std::string>("~abort_topic", "/abort");
  abort_sub_ =
      nodeHandle().subscribe<ds_core_msgs::Abort>(abort_topic, 1, boost::bind(&SentryMc::onAbortMsg, this, _1));
  std::string batman_topic = ros::param::param<std::string>("~batman_topic", "/batman");
  battery_sub_ =
      nodeHandle().subscribe<ds_hotel_msgs::BatMan>(batman_topic, 1, boost::bind(&SentryMc::onBatmanMsg, this, _1));
  std::string main_htp_topic = ros::param::param<std::string>("~main_htp_topic", "/main_htp");
  main_htp_sub_ = nodeHandle().subscribe<ds_hotel_msgs::HTP>(main_htp_topic, 1,
                                                             boost::bind(&SentryMc::onHtpMsg, this, _1, "3", "52"));
  std::string bat_htp_topic = ros::param::param<std::string>("~bat_htp_topic", "/bat_htp");
  bat_htp_sub_ = nodeHandle().subscribe<ds_hotel_msgs::HTP>(bat_htp_topic, 1,
                                                            boost::bind(&SentryMc::onHtpMsg, this, _1, "2", "26"));
  std::string xr1_topic = ros::param::param<std::string>("~xr1_topic", "/xr1");
  xr1_sub_ = nodeHandle().subscribe<ds_hotel_msgs::XR>(xr1_topic, 1, boost::bind(&SentryMc::onXrMsg, this, _1, "1"));
  std::string xr2_topic = ros::param::param<std::string>("~xr2_topic", "/xr2");
  xr2_sub_ = nodeHandle().subscribe<ds_hotel_msgs::XR>(xr2_topic, 1, boost::bind(&SentryMc::onXrMsg, this, _1, "2"));

  // SS - this was introduced with the MBARI TRN tests during 2020-chadwick to provide a different topic for interaction (SHFTABS)
  std::string jtros_bd_ns = ros::param::param<std::string>("~jtros_bd_topic", "/sentry/mission/backdoor");
  jt_bd_sub_ = nodeHandle().subscribe<std_msgs::String>(jtros_bd_ns, 1, boost::bind(&SentryMc::onJtRosMsg, this, _1));
  
}

void SentryMc::setupPublishers()
{
  ds_base::DsProcess::setupPublishers();

  auto nh = nodeHandle();
  std::string joystick_topic = ros::param::param<std::string>("~joystick_topic", "/joystick");
  joystick_pub_ = nodeHandle().advertise<ds_nav_msgs::AggregatedState>(joystick_topic, 1);
  std::string shift_topic = ros::param::param<std::string>("~tracks_shift_topic", "/shift");
  shift_pub_ = nodeHandle().advertise<ds_nav_msgs::Shift>(shift_topic, 1);
  std::string mevt_topic = ros::param::param<std::string>("~mevt_topic", "/mevt");
  mevt_jtros_pub_ = nodeHandle().advertise<ds_core_msgs::StringStamped>(mevt_topic, 1);
  std::string reson_topic = ros::param::param<std::string>("~reson_topic", "/reson");
  reson_jtros_pub_ = nodeHandle().advertise<std_msgs::String>(reson_topic, 1);
}

void SentryMc::setupServices()
{
  ds_base::DsProcess::setupServices();

  auto nh = nodeHandle();
  std::string xr1_service = ros::param::param<std::string>("~xr1_service", "/sentry/sail/grd/xr1/cmd");
  xr1_cmd_srv_ = nh.serviceClient<sentry_msgs::XRCmd>(xr1_service);
  std::string xr2_service = ros::param::param<std::string>("~xr2_service", "/sentry/sail/grd/xr2/cmd");
  xr2_cmd_srv_ = nh.serviceClient<sentry_msgs::XRCmd>(xr2_service);
  std::string servo_fwd_service =
      ros::param::param<std::string>("~servo_fwd_service", "/sentry/actuators/servo_fwd/ctrl_cmd");
  servo_fwd_cmd_srv_ = nh.serviceClient<sentry_msgs::SailServoCmd>(servo_fwd_service);
  std::string servo_aft_service =
      ros::param::param<std::string>("~servo_aft_service", "/sentry/actuators/servo_aft/ctrl_cmd");
  servo_aft_cmd_srv_ = nh.serviceClient<sentry_msgs::SailServoCmd>(servo_aft_service);
  std::string pwr_service = ros::param::param<std::string>("~pwr_service", "/sentry/sail/grd/pwr/cmd");
  pwr_cmd_srv_ = nh.serviceClient<sentry_msgs::PWRCmd>(pwr_service);
  std::string abort_service =
      ros::param::param<std::string>("~abort_supervisor_service", "/sentry/abort_supervisor/cmd");
  abort_srv_ = nh.serviceClient<ds_hotel_msgs::AbortCmd>(abort_service);
  std::string heading_service = ros::param::param<std::string>(
      "~autoheading_service", "/sentry/active_controller/enable_closed_loop_heading_control");
  hdg_srv_ = nh.serviceClient<ds_hotel_msgs::AbortCmd>(heading_service);
  std::string reset_dvl_service =
      ros::param::param<std::string>("~reset_dvl_service", "/sentry/nav/deadreck/reset_dvl");
  reset_dvl_srv_ = nh.serviceClient<ds_nav_msgs::ResetDvl>(reset_dvl_service);
  std::string init_ballast_service =
      ros::param::param<std::string>("~initiate_ballast_service", "/sentry/controllers/ballast/initiate_ballast_test");
  init_ballast_srv_ = nh.serviceClient<sentry_msgs::InitiateBallastTest>(init_ballast_service);

  auto thruster_service =
      ros::param::param<std::string>("~thruster_enable_fwd_stbd", "/sentry/acutators/thruster_fwd_stbd/motor_enable");
  thruster_enable_srv_[0] = nh.serviceClient<std_srvs::SetBool>(thruster_service);

  thruster_service =
      ros::param::param<std::string>("~thruster_enable_aft_stbd", "/sentry/acutators/thruster_aft_stbd/motor_enable");
  thruster_enable_srv_[1] = nh.serviceClient<std_srvs::SetBool>(thruster_service);

  thruster_service =
      ros::param::param<std::string>("~thruster_enable_fwd_port", "/sentry/acutators/thruster_fwd_port/motor_enable");
  thruster_enable_srv_[2] = nh.serviceClient<std_srvs::SetBool>(thruster_service);

  thruster_service =
      ros::param::param<std::string>("~thruster_enable_aft_port", "/sentry/acutators/thruster_aft_port/motor_enable");
  thruster_enable_srv_[3] = nh.serviceClient<std_srvs::SetBool>(thruster_service);

  auto syprid_service =
      ros::param::param<std::string>("~syprid_srv", "/sentry/pkz/syprid_cmd");
  syprid_srv_ = nh.serviceClient<sentry_msgs::SypridCmd>(syprid_service);

  auto supr_service =
      ros::param::param<std::string>("~supr_srv", "/sentry/samplers/supr/supr_cmd");
  supr_srv_ = nh.serviceClient<ds_supr::SuprCmd>(supr_service);

  const auto ajoy_service =
      ros::param::param<std::string>("~timed_joystick_service", "/sentry/goals/timed_joy/timed_joystick_command");
  timed_joystick_goal_srv_ = nh.serviceClient<sentry_msgs::TimedJoystickService>(ajoy_service);
  ROS_INFO_STREAM("Waiting 10s for TimedJoystickService on '" << ajoy_service << "'");
  if (!timed_joystick_goal_srv_.waitForExistence(ros::Duration(10)))
  {
    ROS_FATAL_STREAM("Timed out waiting for TimedJoystickService on '" << ajoy_service << "'");
    ROS_BREAK();
  }
}

void SentryMc::setupActions() {
  const auto kctrl_command_action =
    ros::param::param<std::string>("~kctrl_command_action", "/sentry/sonars/kongsberg/kctrl/kctrl_cmd");
  
  kctrl_command_action_client_.reset(new actionlib::ActionClient<ds_kctrl_msgs::KctrlCmdAction>(kctrl_command_action, asio()->callbackQueue()));

  em2040_system_id_ = ros::param::param<std::string>("~em2040_system_id", "EM2040_130");
  
  // Can't use ActionClient::waitForActionServerToStart due to following warning:
  //
  // NOTE: Using this call in a single threaded ROS application, or any
  // application where the action client's callback queue is not being
  // serviced, will not work. Without a separate thread servicing the queue, or
  // a multi-threaded spinner, there is no way for the client to tell whether
  // or not the server is up because it can't receive a status message.
  //
  // so we need to roll our own polling check. 
  auto t_timeout = ros::Time::now() + ros::Duration(10.0);
  auto poll_rate = ros::Rate(10.0);
  while(ros::Time::now() < t_timeout) {
    asio()->io_service.poll();
    ros::spinOnce();
    if (kctrl_command_action_client_->isServerConnected()) {
      ROS_INFO_STREAM("Connected to kctrl command action: " << kctrl_command_action);
      break;
    }
    ROS_DEBUG_STREAM("Waiting to connect to kctrl command action server on " << kctrl_command_action);
    poll_rate.sleep();
  }
  if(!kctrl_command_action_client_->isServerConnected()) {
    ROS_ERROR_STREAM("Failed to connect to kctrl command action server on " << kctrl_command_action << " after 10 seconds");
  }
 
  const auto kloggerd_command_action =
    ros::param::param<std::string>("~kloggerd_command_action", "/sentry/sonars/kongsberg/kloggerd/kloggerd_cmd");

  kloggerd_command_action_client_.reset(new actionlib::ActionClient<ds_kloggerd_msgs::KloggerdCmdAction>(kloggerd_command_action, asio()->callbackQueue()));
  
  t_timeout = ros::Time::now() + ros::Duration(10.0);
  while(ros::Time::now() < t_timeout) {
    asio()->io_service.poll();
    ros::spinOnce();
    if (kloggerd_command_action_client_->isServerConnected()) {
      ROS_INFO_STREAM("Connected to kloggerd command action: " << kloggerd_command_action);
      break;
    }
    ROS_DEBUG_STREAM("Waiting to connect to kloggerd command action server on " << kloggerd_command_action);
    poll_rate.sleep();
  }
  if(!kloggerd_command_action_client_->isServerConnected()) {
    ROS_ERROR_STREAM("Failed to connect to kloggerd command action server on " << kloggerd_command_action << " after 10 seconds");
  }
} 
}
